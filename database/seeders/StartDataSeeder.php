<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\Fine;
use App\Models\Loan;
use App\Models\User;
use DateTime;
use Illuminate\Database\Seeder;

class StartDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book = Book::create([
            'title' => "Harry Potter and the Philosopher's Stone",
            'publication_date'  => "1997-06-26",
            'copies_owned'  => 144
        ]);
        $book->authors()->attach(Author::where('name', 'J. K. Rowling')->first()->id);
        $book->categories()->attach(1);

        $book2 = Book::create([
            'title' => "The Chronicles of Narnia",
            'publication_date'  => "1956-01-01",
            'copies_owned'  => 140
        ]);
        $book2->authors()->attach(Author::where('name', 'C. S. Lewis')->first()->id);
        $book2->categories()->attach(1);
        $book2->categories()->attach(2);

        $loan1 = Loan::create([
            'book_id'   => Book::inRandomOrder()->first()->id,
            'user_id'   => User::inRandomOrder()->first()->id,
            'staff_id'  => User::inRandomOrder()->first()->id,
            'loan_date' => date('Y-m-d', strtotime("-1 months")),
            'expected_return_date'  => date('Y-m-d', strtotime("-3 weeks")),
            'return_date'   => date('Y-m-d', strtotime("-27 days")),
        ]);

        $loan2 = Loan::create([
            'book_id'   => Book::inRandomOrder()->first()->id,
            'user_id'   => User::inRandomOrder()->first()->id,
            'staff_id'  => User::inRandomOrder()->first()->id,
            'loan_date' => date('Y-m-d', strtotime("-1 months")),
            'expected_return_date'  => date('Y-m-d', strtotime("-3 weeks")),
            'return_date'   => date('Y-m-d', strtotime("-25 days")),
        ]);

        $loan3 = Loan::create([
            'book_id'   => Book::inRandomOrder()->first()->id,
            'user_id'   => User::inRandomOrder()->first()->id,
            'staff_id'  => User::inRandomOrder()->first()->id,
            'loan_date' => date('Y-m-d', strtotime("-1 months")),
            'expected_return_date'  => date('Y-m-d', strtotime("-3 weeks")),
            'return_date'   => date('Y-m-d', strtotime("-18 days")),
        ]);
        $fdate = $loan3->expected_return_date;
        $tdate = $loan3->return_date;
        $datetime1 = new DateTime($fdate);
        $datetime2 = new DateTime($tdate);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        $fine = Fine::create([
            'user_id'   => $loan3->user_id,
            'loan_id'   => $loan3->id,
            'fine_date' => date('Y-m-d'),
            'fine_amount'   => 5000*$days,
        ]);

        $loan3 = Loan::create([
            'book_id'   => Book::inRandomOrder()->first()->id,
            'user_id'   => User::inRandomOrder()->first()->id,
            'staff_id'  => User::inRandomOrder()->first()->id,
            'loan_date' => date('Y-m-d', strtotime("-18 days")),
            'expected_return_date'  => date('Y-m-d', strtotime("-11 days")),
            'return_date'   => null,
        ]);
    }
}
