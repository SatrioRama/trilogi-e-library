<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Satrio Rama',
            'email' => 'satrio.rama@trilogi.ac.id',
            'role' => 'superadmin',
            'password' => bcrypt('password')
        ]);

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@trilogi.ac.id',
            'role' => 'admin',
            'password' => bcrypt('password')
        ]);

        $user2 = User::create([
            'name' => 'Staff',
            'email' => 'staff@trilogi.ac.id',
            'role' => 'staff',
            'password' => bcrypt('password')
        ]);

        $user3 = User::create([
            'name' => 'User',
            'role' => 'user',
            'email' => 'user@trilogi.ac.id',
            'password' => bcrypt('password')
        ]);
    }
}
