<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fantasy = Category::create([
            'name' => 'Fantasy'
        ]);

        $scifi = Category::create([
            'name' => 'Sci-Fi'
        ]);
    }
}
