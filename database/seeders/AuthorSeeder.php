<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rowling = Author::create([
            'name' => 'J. K. Rowling'
        ]);

        $lewis = Author::create([
            'name' => 'C. S. Lewis'
        ]);
    }
}
