<!DOCTYPE html>
<html lang="en">
	<head>
		<title>E-Library Trilogi - @yield('title')</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{asset('src/media/logos/favicon.ico')}}" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
		<link href="{{asset('src/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('src/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        @yield('content-css')
	</head>
	<body id="kt_body" class="app-blank app-blank bgi-size-cover bgi-position-center bgi-no-repeat">
		<script>var defaultThemeMode = "light"; var themeMode; if ( document.documentElement ) { if ( document.documentElement.hasAttribute("data-theme-mode")) { themeMode = document.documentElement.getAttribute("data-theme-mode"); } else { if ( localStorage.getItem("data-theme") !== null ) { themeMode = localStorage.getItem("data-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-theme", themeMode); }</script>
		<div class="d-flex flex-column flex-root" id="kt_app_root">
			<style>body { background-image: url("{{asset('src/media/auth/bg10.jpeg')}}"); } [data-theme="dark"] body { background-image: url("{{asset('src/media/auth/bg10-dark.jpeg')}}"); }</style>
			<div class="d-flex flex-column flex-lg-row flex-column-fluid">
				<div class="d-flex flex-lg-row-fluid">
					<div class="d-flex flex-column flex-center pb-0 pb-lg-10 p-10 w-100">
                        <img class="theme-light-show mx-auto mw-100 w-150px w-lg-300px mb-10 mb-lg-20" src="{{asset('src/media/logos/logo-ramadhanu.png')}}" alt="" />
						<img class="theme-dark-show mx-auto mw-100 w-150px w-lg-300px mb-10 mb-lg-20" src="{{asset('src/media/logos/logo-ramadhanu.png')}}" alt="" />
						<h1 class="text-gray-800 fs-2qx fw-bold text-center mb-7">E-Library ByRama.com</h1>
						<div class="text-gray-600 fs-base text-center fw-semibold">Prototipe ini dibuat oleh
						<a href="https://www.byrama.com/" class="opacity-75-hover text-primary me-1">Satrio Ramadhanu Priambodo</a>
						<br />Sebagai Tugas
						<a href="#" class="opacity-75-hover text-primary me-1">Web Pemrograman Lanjut</a></div>
					</div>
				</div>
				<div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12">
					@yield('content')
				</div>
			</div>
		</div>
        @yield('content-script')
        <script src="{{asset('src/plugins/global/plugins.bundle.js')}}"></script>
		<script src="{{asset('src/js/scripts.bundle.js')}}"></script>
	</body>
</html>
