@extends('auth.template.auth')

@section('title', 'Log-In')

@section('content')
<div class="bg-body d-flex flex-center rounded-4 w-md-600px p-10">
    @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
    @endif
    <div class="w-md-400px">
        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="text-center mb-11">
                <h1 class="text-dark fw-bolder mb-3">Sign In</h1>
                <div class="text-gray-500 fw-semibold fs-6">ByRama E-Library</div>
            </div>
            <div class="fv-row mb-8">
                {{-- <input type="text" placeholder="{{ __('Email') }}" name="email" autocomplete="off" value="{{old('email')}}" required autofocus class="form-control bg-transparent" /> --}}
                <input type="text" placeholder="{{ __('Email') }}" name="email" autocomplete="off" required autofocus class="form-control bg-transparent" />
            </div>
            <div class="fv-row mb-3">
                {{-- <input type="password" placeholder="{{ __('Password') }}" name="password" autocomplete="off" required class="form-control bg-transparent" /> --}}
                <input type="password" placeholder="{{ __('Password') }}" name="password" autocomplete="off" required class="form-control bg-transparent" />
            </div>
            <div class="d-grid mb-10">
                <button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
                    <span class="indicator-label">Sign In</span>
                    <span class="indicator-progress">Please wait...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
            <div class="text-gray-500 text-center fw-semibold fs-6">Not a Member yet?
            <a href="{{ route('register') }}" class="link-primary">Sign up</a></div>
        </form>
    </div>
</div>
@endsection

@section('content-script')
    <script src="{{asset('src/js/custom/authentication/sign-in/general.js')}}"></script>
@endsection
