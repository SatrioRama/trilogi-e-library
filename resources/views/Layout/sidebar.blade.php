<div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
    <div class="menu-item">
        <a class="menu-link {{ (request()->is('dashboard')) ? 'active' : '' }}" href="{{route('dashboard')}}">
            <span class="menu-icon">
                <lord-icon
                    src="https://cdn.lordicon.com/zeabctil.json"
                    trigger="hover"
                    style="width:25px;height:25px">
                </lord-icon>
            </span>
            <span class="menu-title">Dashboards</span>
        </a>
    </div>
</div>
<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
    <span class="menu-link {{ (request()->is('books')) ? 'active' : '' }}">
        <lord-icon
            src="https://cdn.lordicon.com/ttioogfl.json"
            trigger="hover"
            style="width:25px;height:25px">
        </lord-icon>
        <span class="menu-title">Books</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item">
            <a class="menu-link" href="{{route('books.create')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">Create</span>
            </a>
        </div>
        <div class="menu-item">
            <a class="menu-link" href="{{route('books.index')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">List</span>
            </a>
        </div>
    </div>
</div>
<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
    <span class="menu-link {{ (request()->is('loans')) ? 'active' : '' }}">
        <lord-icon
            src="https://cdn.lordicon.com/cllunfud.json"
            trigger="hover"
            style="width:25px;height:25px">
        </lord-icon>
        <span class="menu-title">Loans</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item">
            <a class="menu-link" href="{{route('loans.index')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">List</span>
            </a>
        </div>
    </div>
</div>
<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
    <span class="menu-link {{ (request()->is('authors')) ? 'active' : '' }}">
        <lord-icon
            src="https://cdn.lordicon.com/alzqexpi.json"
            trigger="hover"
            style="width:25px;height:25px">
        </lord-icon>
        <span class="menu-title">Author</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        @if (in_array(auth()->user()->role, ['superadmin', 'admin', 'staff']))
            <div class="menu-item">
                <a class="menu-link" href="{{route('authors.create')}}">
                    <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span class="menu-title">Create</span>
                </a>
            </div>
        @endif
        <div class="menu-item">
            <a class="menu-link" href="{{route('authors.index')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">List</span>
            </a>
        </div>
    </div>
</div>
<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
    <span class="menu-link {{ (request()->is('categories')) ? 'active' : '' }}">
        <lord-icon
            src="https://cdn.lordicon.com/dimghest.json"
            trigger="hover"
            style="width:25px;height:25px">
        </lord-icon>
        <span class="menu-title">Category</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        @if (in_array(auth()->user()->role, ['superadmin', 'admin', 'staff']))
            <div class="menu-item">
                <a class="menu-link" href="{{route('categories.create')}}">
                    <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span class="menu-title">Create</span>
                </a>
            </div>
        @endif
        <div class="menu-item">
            <a class="menu-link" href="{{route('categories.index')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">List</span>
            </a>
        </div>
    </div>
</div>
<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
    <span class="menu-link {{ (request()->is('fines')) ? 'active' : '' }}">
        <lord-icon
            src="https://cdn.lordicon.com/pqxdilfs.json"
            trigger="hover"
            style="width:25px;height:25px">
        </lord-icon>
        <span class="menu-title">Fine</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item">
            <a class="menu-link" href="{{route('fines.index')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">List</span>
            </a>
        </div>
    </div>
</div>
<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
    <span class="menu-link {{ (request()->is('finepayments')) ? 'active' : '' }}">
        <lord-icon
            src="https://cdn.lordicon.com/pqxdilfs.json"
            trigger="hover"
            style="width:25px;height:25px">
        </lord-icon>
        <span class="menu-title">Payment</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item">
            <a class="menu-link" href="{{route('finepayments.index')}}">
                <span class="menu-bullet">
                    <span class="bullet bullet-dot"></span>
                </span>
                <span class="menu-title">List</span>
            </a>
        </div>
    </div>
</div>
@if (in_array(auth()->user()->role, ['superadmin', 'admin']))
    <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
        <span class="menu-link {{ (request()->is('users')) ? 'active' : '' }}">
            <lord-icon
                src="https://cdn.lordicon.com/dqxvvqzi.json"
                trigger="hover"
                style="width:25px;height:25px">
            </lord-icon>
            <span class="menu-title">Users</span>
            <span class="menu-arrow"></span>
        </span>
        <div class="menu-sub menu-sub-accordion">
            @if (in_array(auth()->user()->role, ['superadmin', 'admin']))
                <div class="menu-item">
                    <a class="menu-link" href="{{route('users.create')}}">
                        <span class="menu-bullet">
                            <span class="bullet bullet-dot"></span>
                        </span>
                        <span class="menu-title">Create</span>
                    </a>
                </div>
            @endif
            <div class="menu-item">
                <a class="menu-link" href="{{route('users.index')}}">
                    <span class="menu-bullet">
                        <span class="bullet bullet-dot"></span>
                    </span>
                    <span class="menu-title">List</span>
                </a>
            </div>
        </div>
    </div>
@endif
