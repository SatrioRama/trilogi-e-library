@extends('layout.layout')

@section('title', 'Edit Book')

@section('content-css')
@endsection

@section('content')
<div class="content flex-row-fluid" id="kt_content">
    <form action="{{route('books.update', $data->id)}}" class="form" id="form-edit" method="POST" enctype="multipart/form-data">
    @method('PUT')
    @csrf
        <div class="row g-7">
            <div class="col-xl-4">
                <div class="card card-flush" id="kt_contacts_main">
                    <div class="card-header pt-7" id="kt_chat_contacts_header">
                        <div class="card-title">
                            <h2>Book</h2>
                        </div>
                    </div>
                    <div class="card-body pt-5">
                        <div class="fv-row mb-7">
                            <label class="fs-6 fw-semibold form-label mt-3">
                                <span class="required">Title</span>
                            </label>
                            <input type="text" class="form-control form-control-solid" name="title" value="{{$data->title}}" />
                        </div>
                        <div class="fv-row mb-7">
                            <label class="fs-6 fw-semibold form-label mt-3">
                                <span class="required">Author</span>
                            </label>
                            <select class="form-select form-select-solid" name="author_id[]" multiple="multiple" data-control="select2" data-placeholder="Select an option">
                                <option></option>
                                @foreach ($authors as $author)
                                    <option value="{{$author->id}}" @if (in_array($author->id, $data->authors->pluck('id')->toArray())) selected @endif>{{$author->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="card card-flush h-lg-100" id="kt_contacts_main">
                    <div class="card-body pt-5">
                        <div class="fv-row mb-7">
                            <label class="fs-6 fw-semibold form-label mt-3">
                                <span>Category</span>
                            </label>
                            <select class="form-select form-select-solid" name="category_id[]" multiple="multiple" data-control="select2" data-placeholder="Select an option">
                                <option></option>
                                @foreach ($categories as $category)
                                    <option value="{{$category->id}}" @if (in_array($category->id, $data->categories->pluck('id')->toArray())) selected @endif>{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="fs-6 fw-semibold form-label mt-3">
                                <span>Publication Date</span>
                            </label>
                            <input class="form-control form-control-solid" value="{{$data->publication_date}}" name="publication_date" placeholder="Pick a date" id="kt_datepicker_2"/>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="fs-6 fw-semibold form-label mt-3">
                                <span>Copies Owned</span>
                            </label>
                            <input type="number" value="{{$data->copies_owned}}" class="form-control form-control-solid" name="copies_owned"/>
                        </div>
                        <div class="fv-row mb-7">
                            <a href="{{route('books.index')}}" class="btn btn-secondary btn-hover-scale me-5">Cancle</a>
                            <a href="#" id="btn-submit" class="btn btn-success btn-hover-scale me-5">Submit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('content-script')
<script>
    $("#kt_datepicker_2").flatpickr({
        dateFormat: "d-m-Y",
    });

    $('#btn-submit').on('click', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: "Create Data !",
            text: "Are you sure want to create this data ?",
            icon: "warning",
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: "Yes, Create",
            cancelButtonText: 'Nope, cancel it',
            reverseButtons: true,
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: 'btn btn-secondary',
            },
            buttonsStyling: !1
        }).then((function(result) {
            if (result.value) {
                $('#form-edit').submit();
            }
        }));
    });
</script>
@endsection
