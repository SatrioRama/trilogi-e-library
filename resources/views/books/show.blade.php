@extends('layout.layout')

@section('title', 'Book - '.$data->title)

@section('content-css')
@endsection
@php
    $files = glob('src/media/books/*.*');
    $file = array_rand($files);
@endphp
@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$data->title}}</h3>
                <div class="card-toolbar">
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-primary">Back</a>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex flex-wrap flex-sm-nowrap">
                    <div class="me-7 mb-4">
                        <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                            <img src="{{asset($files[$file])}}" alt="{{$data->title}}">
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="mb-2">
                            <div class="mb-2 text-center">
                                <div class="row">
                                    <div class="col-md-6 fs-2">Title</div>
                                    <div class="col-md-6 fs-2">{{$data->title}}</div>
                                </div>
                            </div>
                            <div class="separator my-2"></div>
                            <div class="mb-2 text-center">
                                <div class="row">
                                    <div class="col-md-6 fs-2">Author</div>
                                    <div class="col-md-6 fs-2">
                                        @foreach ($data->authors as $author)
                                            {{$author->name}}
                                            @if (!$loop->last)
                                            <br>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="separator my-2"></div>
                            <div class="mb-2 text-center">
                                <div class="row">
                                    <div class="col-md-6 fs-2">Category</div>
                                    <div class="col-md-6 fs-2">
                                        @foreach ($data->categories as $category)
                                            {{$category->name}}
                                            @if (!$loop->last)
                                            <br>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="separator my-2"></div>
                            <div class="mb-2 text-center">
                                <div class="row">
                                    <div class="col-md-6 fs-2">Date Release</div>
                                    <div class="col-md-6 fs-2">
                                        {{date('d-m-Y', strtotime($data->publication_date))}}
                                    </div>
                                </div>
                            </div>
                            <div class="separator my-2"></div>
                            <div class="mb-2 text-center">
                                <div class="row">
                                    <div class="col-md-6 fs-2">Copies Owned by Library</div>
                                    <div class="col-md-6 fs-2">
                                        {{$data->copies_owned}} Book(s)
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="separator my-2"></div>
                        <div class="mb-2">
                            <form action="{{route('loans.store')}}" id="form-create" method="post">
                            @csrf
                                <input type="hidden" name="book_id" value="{{$data->id}}">
                                <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                                <input type="hidden" name="loan_date" value="{{date('d-m-Y', strtotime(today()))}}">
                                <a href="#" id="btn-submit" class="btn btn-success btn-hover-scale me-5 col-md-12">Loan this Book</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-script')
<script>
    $('#btn-submit').on('click', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: "Create Data !",
            text: "Are you sure want to loan this book ?",
            icon: "warning",
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: "Yes, Create",
            cancelButtonText: 'Nope, cancel it',
            reverseButtons: true,
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: 'btn btn-secondary',
            },
            buttonsStyling: !1
        }).then((function(result) {
            if (result.value) {
                $('#form-create').submit();
            }
        }));
    });
</script>
@endsection
