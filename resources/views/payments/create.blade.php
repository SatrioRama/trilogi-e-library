@extends('layout.layout')

@section('title', 'Create Author')

@section('content-css')
@endsection

@section('content')
<div class="content flex-row-fluid" id="kt_content">
    <form action="{{route('authors.store')}}" class="form" id="form-create" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="row g-7">
            <div class="col-xl-12">
                <div class="card card-flush" id="kt_contacts_main">
                    <div class="card-header pt-7" id="kt_chat_contacts_header">
                        <div class="card-title">
                            <h2>Author</h2>
                        </div>
                    </div>
                    <div class="card-body pt-5">
                        <div class="fv-row mb-7">
                            <label class="fs-6 fw-semibold form-label mt-3">
                                <span class="required">Name</span>
                            </label>
                            <input type="text" class="form-control form-control-solid" name="name" value="" />
                        </div>
                        <div class="fv-row mb-7">
                            <a href="{{route('authors.index')}}" class="btn btn-secondary btn-hover-scale me-5">Cancle</a>
                            <a href="#" id="btn-submit" class="btn btn-success btn-hover-scale me-5">Submit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('content-script')
<script>
    $('#btn-submit').on('click', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: "Create Data !",
            text: "Are you sure want to create this data ?",
            icon: "warning",
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: "Yes, Create",
            cancelButtonText: 'Nope, cancel it',
            reverseButtons: true,
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: 'btn btn-secondary',
            },
            buttonsStyling: !1
        }).then((function(result) {
            if (result.value) {
                $('#form-create').submit();
            }
        }));
    });
</script>
@endsection
