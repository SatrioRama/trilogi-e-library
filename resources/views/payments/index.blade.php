@extends('layout.layout')

@section('title', 'Fine Payments')

@section('content-css')
<link href="{{asset('src/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <div class="d-flex align-items-center position-relative my-1">
                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                            </svg>
                        </span>
                        <input type="text" id="search-loan" class="form-control form-control-solid w-250px ps-14" placeholder="Search Payment" />
                    </div>
                </div>
                <div class="card-toolbar">
                    <div class="menu menu-rounded menu-column menu-gray-600 menu-state-bg fw-semibold w-auto" data-kt-menu="true">
                        <div class="menu-item" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                            <button class="menu-link btn btn-light-secondary py-3">
                                <span class="menu-icon">
                                    <i class="bi bi-download fs-3"></i>
                                </span>
                                <span class="menu-title">Export</span>
                                <span class="menu-arrow"></span>
                            </button>
                            <div class="menu-sub menu-sub-dropdown p-3 w-100px">
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportCopy">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Copy</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportExcel">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Excel</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportPDF">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">PDF</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportCsv">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">CSV</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body py-4">
                <table class="table align-middle table-row-dashed fs-6 gy-5" id="table_payment">
                    <thead>
                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                            <th class="min-w-125px">Member</th>
                            <th class="min-w-125px">Books</th>
                            <th class="min-w-125px">Loan Date</th>
                            <th class="min-w-125px">Return Date</th>
                            <th class="min-w-125px">Payment Date</th>
                            <th class="min-w-125px">Staff</th>
                            <th class="min-w-125px">Fine Amount</th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">
                        @foreach ($data as $payment)
                            <tr>
                                <td>
                                    {{$payment->user->name}}
                                </td>
                                <td>
                                    {{$payment->fine->loan->books->title}}
                                </td>
                                <td>
                                    {{date('d-m-Y', strtotime($payment->fine->loan->loan_date))}}
                                </td>
                                <td>
                                    {{date('d-m-Y', strtotime($payment->fine->loan->return_date))}}
                                </td>
                                <td>
                                    {{date('d-m-Y', strtotime($payment->payment_date))}}
                                </td>
                                <td>
                                    {{$payment->staff->name}}
                                </td>
                                <td>
                                    Rp{{number_format($payment->fine->fine_amount, 2, ',', '.')}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-script')
<script src="{{asset('src/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script>
    $(document).ready( function () {
        const documentTitle = 'E-Library - Payments';
        var table = $('#table_payment').DataTable({
            title: 'Data export',
            buttons: [
                {
                    extend: 'copyHtml5',
                    title: documentTitle
                },
                {
                    extend: 'excelHtml5',
                    title: documentTitle
                },
                {
                    extend: 'csvHtml5',
                    title: documentTitle
                },
                {
                    extend: 'pdfHtml5',
                    title: documentTitle
                }
            ],
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ]
        });

        $('#search-loan').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $("#exportCopy").on("click", function() {
            table.button( '.buttons-copy' ).trigger();
        });

        $("#exportExcel").on("click", function() {
            table.button( '.buttons-excel' ).trigger();
        });

        $("#exportPDF").on("click", function() {
            table.button( '.buttons-pdf' ).trigger();
        });

        $("#exportCsv").on("click", function() {
            table.button( '.buttons-csv' ).trigger();
        });
    } );
</script>

@endsection
