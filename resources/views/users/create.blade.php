@extends('layout.layout')

@section('title', 'Create User')

@section('content-css')
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <form id="formUser" action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-header border-0 pt-6">
                    <h3 class="card-title">Create User</h3>
                    <div class="card-toolbar">
                        <input type="reset" value="reset" class="btn btn-sm btn-light-danger">
                    </div>
                </div>
                <div class="card-body py-4">
                    <div class="fv-row mb-10">
                        <label class="required fw-semibold fs-6 mb-2">Name</label>
                        <input type="text" name="name" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="{{old('name')}}"  required/>
                    </div>
                    <div class="fv-row mb-10">
                        <label class="required fw-semibold fs-6 mb-2">Email</label>
                        <input type="email" name="email" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="" value="{{old('email')}}" required />
                    </div>
                    <div class="fv-row mb-10" data-kt-password-meter="true">
                        <div class="mb-1">
                            <label class="form-label required fw-semibold fs-6 mb-2">
                                Password
                            </label>

                            <div class="position-relative mb-3">
                                <input class="form-control form-control-lg form-control-solid"
                                    type="password" placeholder="" name="password" autocomplete="off" required />

                                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                                    data-kt-password-meter-control="visibility">
                                    <i class="bi bi-eye-slash fs-2"></i>

                                    <i class="bi bi-eye fs-2 d-none"></i>
                                </span>
                            </div>

                            <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                <div class="flex-grow-1 bg-secondary bg-active-danger rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-warning rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                            </div>
                        </div>

                        <div class="text-muted">
                            Use 8 or more characters with a mix of letters, numbers & symbols.
                        </div>
                    </div>
                    <div class="fv-row mb-10">
                        <div class="mb-1">
                            <label class="form-label required fw-semibold fs-6 mb-2">
                                Confirm Password
                            </label>

                            <div class="position-relative mb-3">
                                <input class="form-control form-control-lg form-control-solid"
                                    type="password" placeholder="" name="password_confirmation" autocomplete="off" required />
                            </div>
                        </div>
                    </div>
                    <div class="fv-row mb-10">
                        <label class="required fw-semibold fs-6 mb-2">Role</label>
                        <select class="form-select form-select-solid" name="role" data-control="select2" data-placeholder="Select Role" required>
                            <option></option>
                            @if (auth()->user()->role == "superadmin")
                                <option value="superadmin">Superadmin</option>
                            @endif
                            <option value="admin">Admin</option>
                            <option value="staff">Staff</option>
                            <option value="user">User</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-center">
                    <a href="{{route('users.index')}}" class="btn btn-light-secondary mx-3">Cancle</a>
                    <a href="#" class="btn btn-light-success mx-3" id="submitUser">Submit</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('content-script')
<script>
    $('#submitUser').on('click', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: "Submit Data !",
            text: "Are you sure want to submit this data ?",
            icon: "warning",
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: "Yes, Submit",
            cancelButtonText: 'Nope, cancel it',
            reverseButtons: true,
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: 'btn btn-danger',
            },
            buttonsStyling: !1
        }).then((function(result) {
            if (result.value) {
                $('#formUser').submit();
            }
        }));
    });
</script>
@endsection
