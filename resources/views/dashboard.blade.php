@extends('layout.layout')

@section('title', 'Dashboard')

@section('content-css')

@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-danger col-md-12">
                    <div class="card-header text-center">
                        <h3 class="card-title">Table Availability Books</h3>
                    </div>
                    <div class="card-body">
                        <table class="table align-middle table-row-dashed fs-6 gy-5" id="table_book">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="min-w-125px">Title</th>
                                    <th class="min-w-125px">Author</th>
                                    <th class="min-w-125px">Category</th>
                                    <th class="min-w-125px">Release Date</th>
                                    <th class="min-w-125px">Available</th>
                                    <th class="min-w-125px">On Loan</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @foreach ($data as $book)
                                    <tr>
                                        <td>
                                            <a href="{{route('books.show', $book->id)}}" class="text-gray-800 text-hover-primary mb-1">{{$book->title}}</a>
                                        </td>
                                        <td>
                                            @foreach ($book->authors as $author)
                                                {{$author->name}}
                                                @if (!$loop->last)
                                                <br>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($book->categories as $category)
                                                {{$category->name}}
                                                @if (!$loop->last)
                                                <br>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div>{{date('d-m-Y', strtotime($book->publication_date))}}</div>
                                        </td>
                                        <td>
                                            <div>{{$book->copies_owned}}</div>
                                        </td>
                                        <td>
                                            <div>{{count($book->loans->where('return_date', null))}}</div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-script')
<script>
    var table = $('#table_book').DataTable({
        title: 'Data export',
        buttons: [
            {
                extend: 'copyHtml5',
                title: documentTitle
            },
            {
                extend: 'excelHtml5',
                title: documentTitle
            },
            {
                extend: 'csvHtml5',
                title: documentTitle
            },
            {
                extend: 'pdfHtml5',
                title: documentTitle
            }
        ],
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ]
    });
</script>
@endsection
