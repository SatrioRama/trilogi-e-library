@extends('layout.layout')

@section('title', 'Category - '.$data->name)

@section('content-css')
@endsection
@php
    $files = glob('src/media/illustrations/sigma-1/*.*');
    $file = array_rand($files);
@endphp
@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$data->name}}</h3>
                <div class="card-toolbar">
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-primary">Back</a>
                </div>
            </div>
            <div class="card-body">
                <div class="d-flex flex-wrap flex-sm-nowrap">
                    <div class="me-7 mb-4">
                        <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                            <img src="{{asset($files[$file])}}" alt="{{$data->name}}">
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                            <div class="d-flex flex-column">
                                <div class="d-flex align-items-center mb-2">
                                    <h3 class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">
                                        {{$data->name}}
                                    </h3>
                                </div>
                                <div class="d-flex align-items-center mb-2">
                                    <h3 class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">
                                        Books : {{(!empty($data->book)) ? count($data->book) : 0}}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-script')
@endsection
