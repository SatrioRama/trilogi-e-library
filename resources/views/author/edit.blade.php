@extends('layout.layout')

@section('title', 'Edit User - '.$data->name)

@section('content-css')
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <form id="form-control" action="{{route('authors.update', $data->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row g-7">
                    <div class="col-xl-12">
                        <div class="card card-flush" id="kt_contacts_main">
                            <div class="card-header pt-7" id="kt_chat_contacts_header">
                                <div class="card-title">
                                    <h2>Author</h2>
                                </div>
                            </div>
                            <div class="card-body pt-5">
                                <div class="fv-row mb-7">
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Name</span>
                                    </label>
                                    <input type="text" class="form-control form-control-solid" name="name" value="{{$data->name}}" />
                                </div>
                                <div class="fv-row mb-7">
                                    <a href="{{route('books.index')}}" class="btn btn-secondary btn-hover-scale me-5">Cancle</a>
                                    <a href="#" id="btn-submit" class="btn btn-success btn-hover-scale me-5">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('content-script')
<script>
    $('#btn-submit').on('click', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: "Submit Data !",
            text: "Are you sure want to submit this data ?",
            icon: "warning",
            buttonsStyling: false,
            showCancelButton: true,
            confirmButtonText: "Yes, Submit",
            cancelButtonText: 'Nope, cancel it',
            reverseButtons: true,
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: 'btn btn-danger',
            },
            buttonsStyling: !1
        }).then((function(result) {
            if (result.value) {
                $('#form-control').submit();
            }
        }));
    });
</script>
@endsection
