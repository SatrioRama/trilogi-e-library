@extends('layout.layout')

@section('title', 'Authors')

@section('content-css')
<link href="{{asset('src/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <div class="d-flex align-items-center position-relative my-1">
                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                            </svg>
                        </span>
                        <input type="text" id="search-book" class="form-control form-control-solid w-250px ps-14" placeholder="Search Authror" />
                    </div>
                </div>
                <div class="card-toolbar">
                    <a href="{{route('authors.create')}}" class="btn btn-light-success"><i class="las la-plus fs-2x"></i>Create</a>
                    &nbsp;
                    <div class="menu menu-rounded menu-column menu-gray-600 menu-state-bg fw-semibold w-auto" data-kt-menu="true">
                        <div class="menu-item" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                            <button class="menu-link btn btn-light-secondary py-3">
                                <span class="menu-icon">
                                    <i class="bi bi-download fs-3"></i>
                                </span>
                                <span class="menu-title">Export</span>
                                <span class="menu-arrow"></span>
                            </button>
                            <div class="menu-sub menu-sub-dropdown p-3 w-100px">
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportCopy">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Copy</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportExcel">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">Excel</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportPDF">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">PDF</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link px-1 py-3" id="exportCsv">
                                        <span class="menu-bullet me-0">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">CSV</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body py-4">
                <table class="table align-middle table-row-dashed fs-6 gy-5" id="table_author">
                    <thead>
                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                            <th class="min-w-125px">Name</th>
                            <th class="min-w-125px">Books</th>
                            <th class="text-end min-w-100px">Actions</th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 fw-semibold">
                        @foreach ($data as $author)
                            <tr>
                                <td>
                                    <a href="{{route('authors.show', $author->id)}}" class="text-gray-800 text-hover-primary mb-1">{{$author->name}}</a>
                                </td>
                                <td>
                                    {{(!empty($author->book)) ? count($author->book) : 0}}
                                </td>
                                <td class="text-end">
                                    <form action="{{route('authors.destroy', $author->id)}}" id="delete_author_{{$author->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('authors.show', $author->id)}}" class="btn btn-light-primary"><i class="las la-eye fs-2x"></i></a>
                                        <a href="{{route('authors.edit', $author->id)}}" class="btn btn-light-warning"><i class="las la-edit fs-2x"></i></a>
                                        @if (in_array(auth()->user()->role, ['superadmin', 'admin']))
                                            <a href="#" data-id="{{$author->id}}" class="btn btn-light-danger btn-delete-book"><i class="las la-trash-alt fs-2x"></i></a>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-script')
<script src="{{asset('src/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script>
    $(document).ready( function () {
        const documentTitle = 'E-Library - Authors';
        var table = $('#table_author').DataTable({
            title: 'Data export',
            buttons: [
                {
                    extend: 'copyHtml5',
                    title: documentTitle
                },
                {
                    extend: 'excelHtml5',
                    title: documentTitle
                },
                {
                    extend: 'csvHtml5',
                    title: documentTitle
                },
                {
                    extend: 'pdfHtml5',
                    title: documentTitle
                }
            ],
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ]
        });

        $('#search-book').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );

        $("#exportCopy").on("click", function() {
            table.button( '.buttons-copy' ).trigger();
        });

        $("#exportExcel").on("click", function() {
            table.button( '.buttons-excel' ).trigger();
        });

        $("#exportPDF").on("click", function() {
            table.button( '.buttons-pdf' ).trigger();
        });

        $("#exportCsv").on("click", function() {
            table.button( '.buttons-csv' ).trigger();
        });

        $('.btn-delete-book').on('click', function () {
            var id = $(this).data('id');
            Swal.fire({
                title: "Delete Data !",
                text: "Are you sure want to delete this data ?",
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "Yes, Delete",
                cancelButtonText: 'Nope, cancel it',
                reverseButtons: true,
                customClass: {
                    confirmButton: "btn btn-success",
                    cancelButton: 'btn btn-danger',
                },
                buttonsStyling: !1
            }).then((function(result) {
                if (result.value) {
                    $('#delete_author_'+id).submit();
                }
            }));
        });


    } );
</script>

@endsection
