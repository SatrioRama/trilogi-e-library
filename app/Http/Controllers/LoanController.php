<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Fine;
use App\Models\Loan;
use DateTime;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Loan::get();

        return view('loans.index')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Loan::create($request->all());

        return redirect()->route('loans.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        $loan->staff_id = auth()->user()->id;
        $loan->expected_return_date = date('Y-m-d', strtotime("+7 day", strtotime($loan->loan_date)));
        $loan->update();
        $book = Book::find($loan->book_id);
        $book->copies_owned -= 1;
        $book->update();

        return redirect()->route('loans.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }

    public function return($id)
    {
        $loan = Loan::find($id);
        $loan->return_date = date('Y-m-d');
        $loan->update();
        $book = Book::find($loan->book_id);
        $book->copies_owned += 1;
        $book->update();
        if ($loan->expected_return_date < $loan->return_date) {
            $fdate = $loan->expected_return_date;
            $tdate = $loan->return_date;
            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            $fine = new Fine();
            $fine->user_id = $loan->user_id;
            $fine->loan_id = $id;
            $fine->fine_date = date('Y-m-d');
            $fine->fine_amount = 5000*$days;
            $fine->save();
            return redirect()->route('fines.index');
        }
        return redirect()->route('loans.index');
    }
}
