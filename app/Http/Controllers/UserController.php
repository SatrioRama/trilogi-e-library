<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin'])) {
            return abort(404);
        }
        $data = (auth()->user()->role == 'superadmin') ? User::get() : User::where('role', '!=', 'superadmin')->get();

        return view('users.index')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin'])) {
            return abort(503);
        }

        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $store = new User();
        $store->name        = $request->name;
        $store->email       = $request->email;
        $store->password    = bcrypt($request->password);
        $store->role        = $request->role;
        $store->save();

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin'])) {
            if (auth()->user()->id != $id) {
                return abort(503);
            }
        }

        $data = User::find($id);

        return view('users.show')->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin'])) {
            if (auth()->user()->id != $id) {
                return abort(503);
            }
        }

        $data = User::find($id);

        return view('users.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin'])) {
            if (auth()->user()->id != $id) {
                return abort(503);
            }
        }

        $update = User::find($id);
        $update->name        = $request->name;
        $update->email       = $request->email;
        $update->password    = bcrypt($request->password);
        $update->role        = $request->role;
        $update->update();

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin'])) {
            return abort(404);
        }

        User::destroy($id);

        return redirect()->route('users.index');
    }
}
