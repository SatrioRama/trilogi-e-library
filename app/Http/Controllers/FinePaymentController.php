<?php

namespace App\Http\Controllers;

use App\Models\FinePayment;
use Illuminate\Http\Request;

class FinePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FinePayment::get();

        return view('payments.index')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FinePayment  $finePayment
     * @return \Illuminate\Http\Response
     */
    public function show(FinePayment $finePayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FinePayment  $finePayment
     * @return \Illuminate\Http\Response
     */
    public function edit(FinePayment $finePayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FinePayment  $finePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinePayment $finePayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FinePayment  $finePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinePayment $finePayment)
    {
        //
    }
}
