<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Book::get();

        return view('books.index')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin', 'staff'])) {
            return abort(503);
        }
        $authors = Author::get();
        $categories = Category::get();
        return view('books.create')->with(compact(['authors', 'categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin', 'staff'])) {
            return abort(503);
        }
        $store = Book::create($request->all());
        foreach ($request->category_id as $category) {
            $store->categories()->attach($category);
        }
        foreach ($request->author_id as $author) {
            $store->authors()->attach($author);
        }

        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $data = $book;

        return view('books.show')->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin', 'staff'])) {
            return abort(503);
        }
        $authors = Author::get();
        $categories = Category::get();
        $data = $book;

        return view('books.edit')->with(compact(['data', 'authors', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        if (!in_array(auth()->user()->role, ['superadmin', 'admin', 'staff'])) {
            return abort(503);
        }
        $update = $book;
        $update->update($request->all());
        $update->categories()->detach();
        $update->authors()->detach();
        foreach ($request->category_id as $category) {
            $update->categories()->attach($category);
        }
        foreach ($request->author_id as $author) {
            $update->authors()->attach($author);
        }

        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
